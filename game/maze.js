//include("./engine/geometry/Vector2d.js");

function Cell() {
    this.visited = false;
    this.footstep = false;
    this.type = Cell.Type.Wall;
}

Cell.Type = {
    Wall: 0,
    Floor: 1
};

function Maze(width, height) {
    this.width = width;
    this.height = height;
    this.cells = [];
    this.ways = [];
    this.debug = true;
    this.breakChance = 4;
    this.neighbourRange = 2;

    //Initialization of the maze
    this.init();

    //Create start pattern
    this.generatePattern();
}

Maze.prototype.init = function() {
    for (var y = 0; y < this.height; ++y) {
        this.cells[y] = [];
        this.ways[y] = [];

        for (var x = 0; x < this.width; ++x) {
            this.cells[y][x] = new Cell();

            if (this.debug) {
                this.ways[y][x] = -1;
            }
        }
    }
};

Maze.prototype.generatePattern = function() {
    for (var y = 0; y < this.height; ++y) {
        for (var x = 0; x < this.width; ++x) {
            //Create border for maze
            var pos = new Vector2d(x, y);
            if (this.isBorder(pos)) {
                this.cells[y][x].type = Cell.Type.Wall;
                continue;
            }

            //Create interior pattern for maze
            if((y%2 != 0 && x%2 != 0)) {
                this.cells[y][x].type = Cell.Type.Floor;
            } else {
                this.cells[y][x].type = Cell.Type.Wall;
            }
        }
    }
};

Maze.prototype.findBetterWay = function(startPos, endPos) {
    /*var diff = new Vector2d(
        (endPos.x - startPos.x),
        (endPos.y - startPos.y)
    );

    var direction = new Vector2d(
        (diff.x/Math.abs(diff.x)), 
        (diff.y/Math.abs(diff.y))
    );

    for (var i = 0; i != diff.x; i += direction.x) {
        for (var y = 0; y < this.height; ++y) {
            var x = startPos.x + i;
            
            if (i != 0) {
                if (!this.cells[y][(x-direction.x)].footstep) {
                    console.log("pass");
                    continue;
                }
            }

            var currentCell = this.cells[y][x];
            if (currentCell.type == Cell.Type.Floor) {
                currentCell.footstep = true;
            }
        }
    }*/

    this.clearFootsteps();

    var stepData = {
        stack: [],
        currentCell: startPos.clone(),
        result: false,
        noWay: false
    }
    
    //Make footstep at start position   
    this.cells[startPos.y][startPos.x].footstep = true;

    //Calculate unvisited cells
    var unvisitedCount = this.calculateUnvisitedCells();
    while(unvisitedCount > 0) {
        //Make step
        this.makeStep(stepData);

        if (stepData.result) {
            if (stepData.currentCell.equals(endPos)) {
                console.log("Found it!");
                break;
            }
        } else if (stepData.noWay) {
            console.log("Here is no way");
            break;   
        }
    }
    
    this.clearVisited();

    return stepData.stack.length;
}

Maze.prototype.makeStep = function(stepData) {
    stepData.result = false;

    //Get neighbours
    var neighbours = this.getNeighbours(stepData.currentCell, 1, true);
    if (neighbours.length > 0) {
        /*if (this.cells[stepData.currentCell.y][stepData.currentCell.x].footstep) {
            stepData.currentCell = stepData.stack.pop();
            return;
        }*/

        //Select random neighbour cell as next
        var nextCellIndex = Math.round(Math.random() * (neighbours.length-1));
        var nextCell = neighbours[nextCellIndex].clone();

        //Remember current cell
        stepData.stack.push(stepData.currentCell.clone());

        //Move to next cell
        stepData.currentCell = nextCell;

        //Mark current cell as visited
        this.cells[stepData.currentCell.y][stepData.currentCell.x].visited = true;
        this.cells[stepData.currentCell.y][stepData.currentCell.x].footstep = true;

        //Mark as successes
        stepData.result = true;
    } else if(stepData.stack.length > 0) {
        this.cells[stepData.currentCell.y][stepData.currentCell.x].footstep = false;
        stepData.currentCell = stepData.stack.pop();
    } else {
        stepData.noWay = true; //Damn, now f**ng way
    }
}

Maze.prototype.create = function(startPos) {
    var stepData = {
        stack: [],
        currentCell: startPos.clone(),
        result: true
    }

    //Calculate unvisited cells
    var unvisitedCount = this.calculateUnvisitedCells();

    var stepIndex = 0;

    while(unvisitedCount > 0) {
        //Save step number for current cell
        if (this.debug && stepData.result) {
            this.ways[stepData.currentCell.y][stepData.currentCell.x] = stepIndex;
            stepIndex++; 
        }

        //Create step
        this.createStep(stepData);

        if (stepData.result) {
            unvisitedCount--;
        }
    }

    this.clearVisited();
    //this.breakRandomWalls();
};


Maze.prototype.createStep = function(stepData) {
    stepData.result = false;

    //Get neighbours
    var neighbours = this.getNeighbours(stepData.currentCell, this.neighbourRange, true);
    
    if (neighbours.length > 0) {
        //Select random neighbour cell as next
        var nextCellIndex = Math.round(Math.random() * (neighbours.length-1));
        var nextCell = neighbours[nextCellIndex].clone();

        //Remember current cell
        stepData.stack.push(stepData.currentCell.clone());

        //Remove wall between current and next cells
        this.removeWall(stepData.currentCell, nextCell);

        //Move to next cell
        stepData.currentCell = nextCell;

        //Mark current cell as visited
        this.cells[stepData.currentCell.y][stepData.currentCell.x].visited = true;

        //Mark as successes
        stepData.result = true;
    } else if(stepData.stack.length > 0) {
        //Back to previous cell
        stepData.currentCell = stepData.stack.pop();
    } else {
        //Move to random unvisited cell
        var unvisited = this.getUnvisitedCells();
        var unvisitedCellIndex = Math.floor(Math.random() * unvisited.length);
        stepData.currentCell = unvisited[unvisitedCellIndex].clone();
    }
};


Maze.prototype.removeWall = function(first, second) {
    //Calculate difference between first and second positions
    var diff = {
        x: second.x - first.x,
        y: second.y - first.y
    };

    //Calculate offset for target
    var offset = {
        x : (diff.x != 0) ? (diff.x / Math.abs(diff.x)) : 0,
        y : (diff.y != 0) ? (diff.y / Math.abs(diff.y)) : 0
    };

    //Calculate final target position
    var target = {
        x: first.x + offset.x,
        y: first.y + offset.y,
    };

    //Mark cell as visited and set id to Floor
    this.cells[target.y][target.x].type = Cell.Type.Floor;
    this.cells[target.y][target.x].visited = true;
};

Maze.prototype.clearVisited = function() {
    for (var y = 0; y < this.height; ++y) {
        for (var x = 0; x < this.width; ++x) {
            this.cells[y][x].visited = false;
        }
    }
};

Maze.prototype.clearFootsteps = function() {
    for (var y = 0; y < this.height; ++y) {
        for (var x = 0; x < this.width; ++x) {
            this.cells[y][x].footstep = false;
        }
    }
};

Maze.prototype.breakRandomWalls = function() {
    for (var y = 0; y < this.height; ++y) {
        for (var x = 0; x < this.width; ++x) {
            //If is not a wall
            if (this.cells[y][x].type != Cell.Type.Wall) {
                continue;
            }

            var pos = new Vector2d(x, y);
            //If wall is a part of border
            if (this.isBorder(pos)) {
                continue;
            }
            
            //If don't split way
            if (!this.isSplitWay(pos)) {
                continue;
            }

            //Break?
            var randomPercent = Utils.generateRandomPercent();
            if (randomPercent < this.breakChance) {
                this.cells[y][x].type = Cell.Type.Floor;
            }
        }
    }
};

Maze.prototype.getNeighbours = function(pos, count, ignoreWalls) {
    var neighbours = [];

    //Collect avaible unvisited cells to array for all directions
    neighbours = neighbours.concat(
        this.getNeighboursByDirection(pos, new Vector2d(count, count), ignoreWalls),
        this.getNeighboursByDirection(pos, new Vector2d(-count, 0), ignoreWalls),
        this.getNeighboursByDirection(pos, new Vector2d(0, -count), ignoreWalls)
    );
 
    return neighbours;
};

Maze.prototype.getNeighboursByDirection = function(pos, direction, ignoreWalls) {
    var neighbours = [];

    //If direction vector have directions for 2 axis
    if ((direction.x != 0) && (direction.y != 0)) {
        //Split combinate axis vector to single axis vector
        neighbours = neighbours.concat(
            this.getNeighboursByDirection(pos, new Vector2d(direction.x, 0), ignoreWalls),
            this.getNeighboursByDirection(pos, new Vector2d(0, direction.y), ignoreWalls)
        );
    //If direction vector have direction for single axis
    } else {
        var horizontal = (direction.x != 0);
        var end = (horizontal) ? (direction.x) : (direction.y);
        var itDirection = end / Math.abs(end);

        if (this.debug) {
            console.debug(
                        "Pos: ", pos.x, pos.y, "\n",
                        "Direction: ", direction.x, direction.y, "\n",
                        "Horizontal: ", horizontal, "\n",
                        "End: ", end, "\n",
                        "It Direction: ", itDirection
            
            );
        }

        for (var it = 0; it != end; it += itDirection) {
            //Calculate offset for current position
            var offset = (it + 1*itDirection);
            
            //Get current position and apply offset
            var current = pos.clone();
            if (horizontal) {
                current.x += offset;
            } else {
                current.y += offset;
            }

            //Break if current position is not inside maze
            if (!this.isInside(current)) {
                if (this.debug) {
                    console.debug("Not inside");
                }

                break;
            }

            //Break if current cell is already visited
            if (this.cells[current.y][current.x].visited) {
                if (this.debug) {
                    console.debug("Already visited");
                }

                break;
            }

            //Add neighbour if current cell is not a wall
            if (!ignoreWalls || this.cells[current.y][current.x].type != Cell.Type.Wall) {
                neighbours.push(current);

                if (this.debug) {
                    console.debug(
                        "Current: ", current.x, current.y, "\n",
                        "Neighbours length: ", neighbours.length, "\n"
                    )
                }
            } else {
                if (this.debug) {
                    console.debug("Stuck in the wall.");
                }
            }
        }
    }

    return neighbours;
};

Maze.prototype.getUnvisitedCells = function() {
    var unvisited = [];

    for (var y = 0; y < this.height; ++y) {
        for (var x = 0; x < this.width; ++x) {
            if (!this.cells[y][x].visited && !this.cells[y][x].type != Cell.Type.Wall) {
                unvisited.push(new Vector2d(x, y));
            }
        }
    }
    
    return unvisited;
};

Maze.prototype.calculateUnvisitedCells = function() {
    var total = 0;;

    for (var y = 0; y < this.height; ++y) {
        for (var x = 0; x < this.width; ++x) {
            if (this.cells[y][x].type != Cell.Type.Wall && !this.cells[y][x].visited) {
                total++;
            }
        }
    }
    
    return total;
};

Maze.prototype.isInside = function(pos) {
    if (pos.x < 0) {
        return false;
    }
    
    if (pos.y < 0) {
        return false;
    } 
    
    if (pos.y >= this.height) {
        return false;
    }

    if (pos.x >= this.width) {
        return false;
    }

    return true;
}

Maze.prototype.isBorder = function(pos) {
    if (pos.x == 0) {
        return true;
    }

    if (pos.y == 0) {
        return true;
    } 
    
    if (pos.y == (this.height - 1)) {
        return true;
    }

    if (pos.x >= (this.width - 1)) {
        return true;
    }

    return false;
}

Maze.prototype.isSplitWay = function(pos) {
    return (this.isSplitWayByX(pos) || this.isSplitWayByY(pos));
}

Maze.prototype.isSplitWayByX = function(pos) {
        var neighbour1 = pos.clone();
        neighbour1.x++;

        if (!this.isInside(neighbour1)) {
            return false;
        }

        if (this.cells[neighbour1.y][neighbour1.x] == Cell.Type.Wall) {
            return false;
        }


        var neighbour2 = pos.clone();
        neighbour2.x--;

        if (!this.isInside(neighbour2)) {
            return false;
        }

        if (this.cells[neighbour2.y][neighbour2.x] == Cell.Type.Wall) {
            return false;
        }

        return true;
}

Maze.prototype.isSplitWayByY = function(pos) {
        var neighbour1 = pos.clone();
        neighbour1.y++;

        if (!this.isInside(neighbour1)) {
            return false;
        }

        if (this.cells[neighbour1.y][neighbour1.x] == Cell.Type.Wall) {
            return false;
        }

        var neighbour2 = pos.clone();
        neighbour2.y--;

        if (!this.isInside(neighbour2)) {
            return false;
        }

        if (this.cells[neighbour2.y][neighbour2.x] == Cell.Type.Wall) {
            return false;
        }

        return true;
}