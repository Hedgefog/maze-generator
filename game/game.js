function Game(render) {
    this.maze;
    this.render = render;
    this.mazeSize = new Size(0, 0);
    this.blockSize = 8;
    this.debug = false;
    this.wayFound = false;
    this.redrawRequired = true;

    this.startPos;
    this.endPos;

    var self = this;
    setInterval(function() {self.tick();}, 1);
}

Game.prototype.createMaze = function(startPos) {
    this.maze = new Maze(this.mazeSize.width, this.mazeSize.height);
    this.maze.debug = this.debug;
    this.maze.create(startPos);
    this.wayFound = false;
    this.redrawRequired = true;
};

Game.prototype.setStart = function(pos) {
    if (this.maze.cells[pos.y][pos.x].type == Cell.Type.Wall) {
        alert("You can't set point in the wall!");
        return;
    }

    this.startPos = pos;
    this.redrawRequired = true;
};

Game.prototype.setEnd = function(pos) {
    if (this.maze.cells[pos.y][pos.x].type == Cell.Type.Wall) {
        alert("You can't set point in the wall!");
        return;
    }

    if (!this.startPos || pos.equals(this.startPos)) {
        alert("End position should be different from the start position");
        return;
    }
    
    this.endPos = pos;
    this.redrawRequired = true;
};

Game.prototype.findBetterWay = function(startPos, endPos) {
    var steps = this.maze.findBetterWay(this.startPos, this.endPos);
    this.startPos = undefined;
    this.endPos = undefined;
    this.wayFound = true;
    this.redrawRequired = true;

    return steps;
};

Game.prototype.addPoint = function(pos) {
    var origin = new Vector2d(pos.x*this.blockSize + this.blockSize/2, pos.y*this.blockSize + this.blockSize/2);
    this.render.drawCircle(origin, this.blockSize/2, "red");
};

Game.prototype.drawStartPos = function() {
    if (this.startPos) {
        this.addPoint(this.startPos);
    }
};

Game.prototype.drawEndPos = function() {
    if (this.endPos) {
        this.addPoint(this.endPos)
    }
};

Game.prototype.drawWay = function() {
    if (!this.maze) {
        return;
    }

    if (!this.wayFound) {
        return;
    }

    for (var y = 0; y < this.maze.height; ++y) {
        for (var x = 0; x < this.maze.width; ++x) {
            var pos = new Vector2d(
                (x*this.blockSize) + (this.blockSize/2), 
                (y*this.blockSize) + (this.blockSize/2)
            );

            if (this.maze.ways[y][x] >= 0) {
                this.render.drawNumber(pos, this.maze.ways[y][x], FontSize);
            }
        }
    }
};

Game.prototype.drawMaze = function() {
    if (!this.maze) {
        return;
    }
    
    for (var y = 0; y < this.maze.height; ++y) {
        for (var x = 0; x < this.maze.width; ++x) {
            var color;
            if (this.maze.cells[y][x].type == Cell.Type.Floor) {
                if (this.maze.cells[y][x].footstep) {
                    color = "yellow";
                } else {
                    color = "lightgray";
                }
            } else if (this.maze.cells[y][x].type == Cell.Type.Wall) {
                color = "black";
            }

            var pos = new Vector2d(x, y);
            this.render.drawBlock(pos, color, this.blockSize);
        }
    }
};

Game.prototype.draw = function() {
    const FontSize = Math.round(this.blockSize*0.375);

    this.render.clear();
    this.drawMaze();
    this.drawWay();
    this.drawStartPos();
    this.drawEndPos();
};

Game.prototype.tick = function() {
    if (!this.redrawRequired) {
        return;
    }

    this.draw();
    console.log("redraw");
    this.redrawRequired = false;
};