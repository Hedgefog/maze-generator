function Vector2d(x, y) {
    if (!y) {
        y = 0;

        if (!x) {
            x = 0;
        }
    }
    
    this.x = x;
    this.y = y;
}

Vector2d.prototype.clone = function() {
  return new Vector2d(this.x, this.y);
}

Vector2d.prototype.subtract = function(other) {
  var vector = new Vector2d(this.x, this.y);
  vector.x -= other.x;
  vector.y -= other.y;
 
  return vector;
};
 
Vector2d.prototype.add = function(other) {
  var vector = new Vector2d(this.x, this.y);
  vector.x += other.x;
  vector.y += other.y;
 
  return vector;
};
 
Vector2d.prototype.multiply = function(other) {
  var vector = new Vector2d(this.x, this.y);
  vector.x *= other.x;
  vector.y *= other.y;
 
  return vector;
};
 
Vector2d.prototype.divide = function(other) {
  var vector = new Vector2d(this.x, this.y);
  vector.x /= other.x;
  vector.y /= other.y;
 
  return vector;
};
 
Vector2d.prototype.equals = function(other) {
  return ((this.x == other.x) && (this.y == other.y));
}
 
Vector2d.prototype.normalize = function() {
  var vector = new Vector2d(this.x, this.y);
  var length = Math.sqrt((vector.x*vector.x) + (vector.y*vector.y));
 
  vector.x /= length;
  vector.y /= length;
 
  return vector;
};