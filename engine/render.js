function Render(canvas, width, height) {
    this.canvas = canvas;
    this.canvas.width = width;
    this.canvas.height = height;
    this.context = this.canvas.getContext('2d');
}

Render.prototype.drawBlock = function(origin, color, size) {
    var start = {
        x: origin.x*size,
        y: origin.y*size,
    }

    var size = {
        width: 1*size,
        height: 1*size,
    }

    this.context.beginPath();
    this.context.rect(start.x, start.y, size.width, size.height);
    this.context.fillStyle = color;
    this.context.fill();
    //context.lineWidth = 7;
    ///context.strokeStyle = color;
    //context.stroke();
}

Render.prototype.drawNumber = function(pos, number, fontSize) {
    var strNumber = number.toString();
    var x = pos.x - Math.round(strNumber.length * fontSize/4);
    var y = pos.y + Math.round(fontSize/2);

    this.context.font = fontSize + "px Arial";
    this.context.beginPath();
    this.context.fillText(strNumber, x, y);
    this.context.fillStyle = "red";
    this.context.fill();
}

Render.prototype.drawCircle = function(pos, radius, color) {
    this.context.beginPath();
    this.context.arc(pos.x, pos.y, radius, 0, 2 * Math.PI, false);
    this.context.fillStyle = color;
    this.context.fill();
}

Render.prototype.clear = function() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
}