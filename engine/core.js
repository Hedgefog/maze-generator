function include(path) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = path;
    document.getElementsByTagName('head')[0].appendChild(script);
}