const MazeStartPos = new Vector2d(0, 0);
const BlockSize = 32;

var g_mazeSize;

var g_canvas;
var g_generateMazeButton;
var g_mazeWidthInput;
var g_mazeHeightInput;

var g_render;
var g_game;

window.onload = function() {
    g_mazeSize = new Size(31, 31);
    
    g_canvas = document.querySelector("#renderCanvas");
    g_generateMazeButton = document.querySelector("#generateMazeButton");
    
    g_mazeWidthInput = document.querySelector("#mazeSize #width");
    g_mazeWidthInput.value = g_mazeSize.width;

    g_mazeHeightInput = document.querySelector("#mazeSize #height");
    g_mazeHeightInput.value = g_mazeSize.height;

    render = new Render(g_canvas, 0, 0);
    g_game = new Game(render);

    g_canvas.onclick = onCanvasClick;
    generateMazeButton.onclick = onGenerateMazeButtonClick;

    generateMaze();
};

function onGenerateMazeButtonClick() {
    generateMaze();
}

function onCanvasClick(event) {
    var x = Math.floor((event.clientX + window.pageXOffset - g_canvas.offsetLeft)/32);
    var y = Math.floor((event.clientY + window.pageYOffset - g_canvas.offsetTop)/32);
    var pos = new Vector2d(x, y);

    if (!g_game.startPos) {
        g_game.setStart(pos);
    } else if(!g_game.endPos) {
        g_game.setEnd(pos);

        if (g_game.endPos) {
            var steps = g_game.findBetterWay();

            setTimeout(function() {
                if (steps > 0) {
                    confirm("Way found in " + steps + " steps!");
                } else {
                    confirm("Can't find the way!");
                }
            }, 1);
        }
    }
}

function generateMaze() {
    if (g_mazeWidthInput.value && g_mazeHeightInput.value) {
        g_mazeSize.width = parseInt(g_mazeWidthInput.value);
        g_mazeSize.height = parseInt(g_mazeHeightInput.value);
    }

    g_canvas.width = g_mazeSize.width*BlockSize;
    g_canvas.height = g_mazeSize.height*BlockSize;

    g_game.blockSize = BlockSize;
    g_game.mazeSize = g_mazeSize;
    g_game.debug = false;
    g_game.createMaze(MazeStartPos);
}